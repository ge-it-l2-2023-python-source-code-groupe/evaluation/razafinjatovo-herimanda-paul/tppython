def Friedman():
    print("-----Nombre de FRIEDMAN-----")
    print(
        f"""
7+3\u2076={7+3**6} ===> est un nombre de Friedman
(3+4)\u2077={(3+4)**3} ===> est un nombre de Friedman
3\u2076 -5 ={3**6-5} ===> n'est pas un nombre de Friedman
(1+2\u2078)*5 ={(1+2**8)*5} ===> est un nombre de Friedman
(2+1\u2078)\u2077={(2+1**8)**7} ===> est un nombre de Friedman
          """
    )


def Prediction():
    print("-----PREDICTION-----")
    print(
        f"""
(1+2)**3 =====̾> {(1+2)**3}
"Da" * 4 ====> {"Da" * 4}
"Da" + 3 ====> can only concatenate str (not "int") to str
("Pa"+"La") * 2 ====> {("Pa"+"La") * 2}
("Da"*4) / 2 ====> unsupported operand type(s) for /: 'str' and 'int'
5/2  ====> {5/2}
5//2 ====> {5 // 2}
5%2  ====> {5%2}
          """
    )


def Prediction_2():
    print("-----PREDICTION 2-----")
    print(
        f"""
str(4) * int("3") ====> {str(4) * int("3")}
int("3") + float("3.2") ====> {int("3") + float("3.2")}
str(3) * float("3.2") ====> can't multiply sequence by non-int of type 'float'
str(3/4) * 2 ====> {str(3/4) * 2}
          """
    )
