def Poly_A():
    print("--A*20--")
    print("--->", "A" * 20)


def Poly_A_Poly_GC():
    print("A*20 + GC *40")
    print("--->", "A" * 20 + "GC" * 40)


def Ecriture_formatee():
    a, b, c = "Salut", 102, 10.318
    print("a\tb\tc")
    print("||\t||\t||")
    print(f"{a} , {b} , {c:.2f}")


def Ecriture_formatee_2():
    perc_GC = ((4500 + 2575) / 14800) * 100

    print("Ecriture formatée ")
    print(
        "Le pourcentage de GC est :{:.0f} %".format(perc_GC),
        "\nLe pourcentage de GC est :{:.1f} %".format(perc_GC),
        "\nLe pourcentage de GC est: {:.2f} %".format(perc_GC),
        "\nLe pourcentage de GC est: {:.3f} %".format(perc_GC),
    )

    print("\nEcriture f-string")
    print(
        f"Le pourcentage de GC est {perc_GC:.0f} %\nLe pourcentage de GC est {perc_GC:.1f} % \nLe pourcentage de GC est {perc_GC:.2f} %  \nLe pourcentage de GC est {perc_GC:.3f} %"
    )
