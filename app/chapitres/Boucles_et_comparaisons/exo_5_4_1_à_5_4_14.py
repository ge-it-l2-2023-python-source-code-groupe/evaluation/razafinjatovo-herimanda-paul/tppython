def Boucle_base():
    liste = ["vache", "souris", "levure", "bacterie"]
    print(f"-----Liste----- {liste}")
    print("")
    print("-----ANSWER-----")
    print("-----Methode 1 : Boucle while-----")
    index = 0
    while index < len(liste):
        print(f"---> {liste[index]}")
        index += 1

    print("-----Methode 2 : Boucle for-----")
    for element in liste:
        print(f"--> {element}")

    print("-----Methode 3 : Boucle for(range,list)-----")
    for element in range(len(liste)):
        print(f"---> {liste[element]}")


def Boucle_jour_semaine():
    semaine = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
    print(f"-----Liste-----{semaine}")
    print("")
    print("-----ANSWER-----")
    print("-----Methode 1 : en utilisant la boucle for-----")
    for jour in semaine:
        print(f"---> {jour}")
    print("")
    print(
        "-----Methode 2 : en utilisant la boucle while et en affichant les jours de week-end-----"
    )
    index = 5
    while index < len(semaine):
        print(f"---> {semaine[index]}")
        index += 1


def Nombre_1_10():
    print("-----ANSWER-----")
    print("nombre = [i for i in range(1,11)]")
    nombre = [i for i in range(1, 11)]
    print(f"--->{nombre}")


def Nombre_pair_imapir():
    impaire = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]
    print(f"Nombre impair ---> {impaire}")
    pair = [i + 1 for i in impaire]
    print(f"Nombre pair à partir de la liste de nombre impair")
    print(f" Pair ---> {pair}")


def Calcul_moyenne():
    print("-----ANSWER-----")
    note = [14, 9, 6, 8, 12]
    print(f"Note ---> {note}")
    somme = 0
    for i in note:
        somme += i

    moyenne = somme / len(note)
    print(f" La moyenne de la classe est ---> {moyenne:.2f}")


def Produit_nombre_consecutif():
    print("-----ANSWER 1-----")
    pair = [i for i in range(2, 21, 2)]
    print(f"Nombre pair de 2 à 20 ---> {pair}")
    print("")
    print("-----ANSWER 2-----")
    print("-----Calcul de nombres consécutifs deux à deux entiers-----")
    tab = []
    prod = 1
    for i in pair:
        prod = i * (i + 2)

        tab.append(prod)
    print(f" ---> {tab} ")


def Triangle():
    print("-----TRIANGLE-----")
    print("-----ANSWER-----")
    print("---> ")
    row = 12
    for j in range(1, row + 1):
        print("*" * j)


def Triangle_inverse():
    print("-----Triangle inversé-----")
    print("-----ANSWER-----")
    print(" --->")
    row = 12
    for j in range(row):
        print("*" * (row - j))


def Triangle_gauche():
    print("-----Triangle gauche-----")
    print("-----ANSWER-----")
    print(" --->")
    row = 12
    for j in range(row):
        print(" " * (row - j) + "*" * j)


def Pyramide():
    print("-----Pyramide-----")
    print("-----ANSWER-----")
    n = 10

    for i in range(1, n + 1):
        for j in range(1, n - i + 1):
            print(" ", end="")

        for k in range(1, 2 * i):
            print("*", end="")

        print()


def Parcour_matrice():
    print("-----Parcours de matrice-----")
    print("-----BOUCLE FOR-----")

    matrice = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

    for ligne in range(len(matrice)):
        for colonne in range(len(matrice[ligne])):
            element = matrice[ligne][colonne]
            print(f"Élément LIGNE:{ligne + 1}  --- COLONNE:{colonne + 1}==== {element}")

    print("")
    print("-----BOUCLE WHILE-----")
    matrice = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

    ligne = 0
    colonne = 0

    while ligne < len(matrice):
        colonne = 0
        while colonne < len(matrice[ligne]):
            element = matrice[ligne][colonne]
            print(f"Élément LIGNE:{ligne + 1}  --- COLONNE:{colonne + 1} = {element}")
            colonne += 1
        ligne += 1


def Parcour_demi_matrice():
    print("-----Parcours Demi matrice sans diagonal-----")
    print("-----ANSWER-----")
    n = int(input("Entrer la taille de la matrice carrée : "))
    total_cases = 0
    print("---> Ligne , Colonne")
    for i in range(n):
        for j in range(i):
            print(f"---> ({i}\t,\t {j}) ")
            total_cases += 1

    print(f"---> Taille de la matrice {n} x {n}")
    print(f"---> Nombre total de cases parcourues : {total_cases}")
    
    for n in range(2, 11):
        total_cases = n * n
    print(f"---> Taille de la matrice {n} x {n}")
    print(f"---> Nombre total de cases parcourues : {total_cases}")


def Saut_de_puce():
    import random

    print("-----SAUT DE PUCE-----")
    print("-----ANSWER-----")
    print("")
    position = 0
    final_position = 5
    jumps = 0

    while position != final_position:
        jump = random.choice([1, -1])
        position += jump
        jumps += 1

    print(f" ---> La puce est arrivée à l'emplacement final en {jumps} sauts.")


def Suite_fibonnacci():
    print("-----SUITE DE FIBONNACCI-----")
    print("-----ANSWER-----")

    n = int(input("entrer un nombre : "))

    a, b = 0, 1

    print(a)
    print(b)

    i = 2
    while i < n:
        c = a + b
        print(c)
        a, b = b, c
        i += 1
