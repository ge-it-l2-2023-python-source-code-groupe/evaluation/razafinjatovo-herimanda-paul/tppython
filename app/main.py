from tools.console import clear
from pynput import keyboard


def on_press(key):
    if key == keyboard.Key.enter:
        return False


def key():
    with keyboard.Listener(on_press=on_press) as Listener:
        print("\n-----Press ENTER----- ")
        Listener.join()
        clear()


def Menu():
    print(
        """\n
    -----MENU-----"""
    )
    print(
        """
          1 - Chap 1 : Variables
          2 - Chap 2 : Affichage
          3 - Chap 3 : Liste
          4 - Chap 4 : Boucles et comparaisons
          5 - Chap 5 : Tests
          6 - Quitter
          """
    )


def chap1():
    print("----- CHAPITRE 1 : VARIABLES -----")
    print(
        """
         1 - Nombres de Friedman
         2 - Prédire le résultat : opérations
         3 - Prédire le résultat : opérations et conversions de types
         4 - Retour au Menu  
          """
    )


def chap2():
    print("----- CHAPITRE 2 : AFFICHAGE -----")
    print(
        """
    1 - Poly-A
    2 - Poly-A et Poly_GC
    3 - Ecriture Formatée 1
    4 - Ecriture formatée 2
    5 - Retour Menu
          """
    )


def chap3():
    print("----- CHAPITRE 3 : LISTE -----")
    print(
        """
    1 - Jours de la semaine
    2 - Saisons
    3 - Table de multiplication de 9 (list, range)
    4 - Nombre de nombres pairs entre [2,10000]
    5 - List & indice
    6 - List & range
    7 - Retour Menu
          """
    )


def chap4():
    print("\n----- CHAPITRE 4 : BOUCLE ET COMPARAISON -----")
    print(
        """
    1 - Boucles de base
    2 - Boucle et jours de la semaine
    3 - Nombres de 1 à 10 sur une ligne
    4 - Nombre pairs et impairs
    5 - Calcul de la moyenne
    6 - Produit de nombres consécutifs
    7 - Triangle
    8 - Triangle inversé 
    9 - Triangle gauche
    10 - Pyramide
    11 - Parcours de matrice
    12 - Parcours de demi-matrice sans la diagonale
    13 - Sauts de puce
    14 - Suite de Fibonacci
    15 - Retour au menu principale
          """
    )


def chap5():
    print("----- CHAPITRE 5 : TESTS -----")
    print(
        """
        1 - Jours de la semaine
        2 - Séquence complémentaire d'un brin d'ADN
        3 - Minimum d'une liste
        4 - Fréquence des acides aminés
        5 - Notes et mention d'un étudiant
        6 - Nombres pairs et impairs
        7 - Conjecture de Syracuse
        8 - Détermination des nombres premiers inférieurs à 100
        9 - Recherche d'un nombre par dichotomie
        10 - Retour au menu principale
            """
    )


"""_summary_
Endpoint 
"""
if __name__ == "__main__":
    from chapitres.Variables.exo_2_11_1_à_2_11_3 import *
    from chapitres.Affichage.exo_3_6_2_à_3_6_5 import *
    from chapitres.Liste.exo_4_10_1_à_4_10_6 import *
    from chapitres.Boucles_et_comparaisons.exo_5_4_1_à_5_4_14 import *
    from chapitres.Tests.exo_6_7_1_à_6_7_10 import *

    while True:
        Menu()
        choix = input("--Please insert your choice--")
        print("")
        clear()
        if choix == "1":
            while True:
                chap1()
                choix_1 = input("--> ")
                print("")
                clear()
                if choix_1 == "1":
                    Friedman()
                    key()
                    continue
                elif choix_1 == "2":
                    Prediction()
                    key()
                    continue
                elif choix_1 == "3":
                    Prediction_2()
                    key()
                    continue
                elif choix_1 == "4":
                    print("--> Retour au Menu \n")
                    key()
                    break

        elif choix == "2":
            while True:
                chap2()
                choix_2 = input("--> ")
                print("")
                clear()
                if choix_2 == "1":
                    Poly_A()
                    key()
                    continue
                elif choix_2 == "2":
                    Poly_A_Poly_GC()
                    key()
                    continue
                elif choix_2 == "3":
                    Ecriture_formatee()
                    key()
                    continue
                elif choix_2 == "4":
                    Ecriture_formatee_2()
                    key()
                    continue
                elif choix_2 == "5":
                    print("--> Retour au Menu \n ")
                    key()
                    break
        elif choix == "3":
            while True:
                chap3()
                choix_3 = input("--> ")
                print("")
                clear()
                if choix_3 == "1":
                    Jour_semaine()
                    key()
                    continue
                elif choix_3 == "2":
                    Saison()
                    key()
                    continue
                elif choix_3 == "3":
                    Table_multiplication_9()
                    key()
                    continue
                elif choix_3 == "4":
                    length_nombre_pair()
                    key()
                    continue
                elif choix_3 == "5":
                    List_indice()
                    key()
                    continue
                elif choix_3 == "6":
                    List_range()
                    key()
                    continue
                elif choix_3 == "7":
                    print("--> Retour au Menu \n")
                    key()
                    break

        elif choix == "4":
            while True:
                chap4()
                choix_4 = input("--> ")
                print("")
                clear()
                if choix_4 == "1":
                    Boucle_base()
                    key()
                    continue
                elif choix_4 == "2":
                    Boucle_jour_semaine()
                    key()
                    continue
                elif choix_4 == "3":
                    Nombre_1_10()
                    key()
                    continue
                elif choix_4 == "4":
                    Nombre_pair()
                    key()
                    continue
                elif choix_4 == "5":
                    Calcul_moyenne()
                    key()
                    continue
                elif choix_4 == "6":
                    Produit_nombre_consecutif()
                    key()
                    continue
                elif choix_4 == "7":
                    Triangle()
                    key()
                    continue
                elif choix_4 == "8":
                    Triangle_inverse()
                    key()
                    continue
                elif choix_4 == "9":
                    Triangle_gauche()
                    key()
                    continue
                elif choix_4 == "10":
                    Pyramide()
                    key()
                    continue
                elif choix_4 == "11":
                    Parcour_matrice()
                    key()
                    continue
                elif choix_4 == "12":
                    Parcour_demi_matrice()
                    key()
                    continue
                elif choix_4 == "13":
                    Saut_de_puce()
                    key()
                    continue
                elif choix_4 == "14":
                    Suite_fibonnacci()
                    key()
                    continue
                elif choix_4 == "15":
                    print("--> Retour au Menu \n ")
                    key()
                    break
        elif choix == "5":
            while True:
                chap5()
                choix_5 = input("--> ")
                print("")
                clear()
                if choix_5 == "1":
                    Jour_semaine()
                    key()
                    continue
                elif choix_5 == "2":
                    Sequence_brin_ADN()
                    key()
                    continue
                elif choix_5 == "3":
                    Minimum_list()
                    key()
                    continue
                elif choix_5 == "4":
                    Frequence_acide_anime()
                    key()
                    continue
                elif choix_5 == "5":
                    Mention_etudiant()
                    key()
                    continue
                elif choix_5 == "6":
                    Nombre_pair()
                    key()
                    continue
                elif choix_5 == "7":
                    Syracuse()
                    key()
                    continue
                elif choix_5 == "8":
                    Nombre_premier()
                    key()
                    continue
                elif choix_5 == "9":
                    Nombre_dichotomie()
                    key()
                    continue
                elif choix_5 == "10":
                    print("--> Retour au Menu \n ")
                    key()
                    break

        elif choix == "6":
            key()
            break
